<?php
require_once("Database.php");
class Category extends Database{
    private $properties;

    public function __set($name, $value)
    {
        $this->properties[$name] = $value;
    }
    
    public function __get($propertyName)
    {
        if(array_key_exists($propertyName, $this->properties))
        {
            return $this->properties[$propertyName];
        }
    }
    public function GetCat()
    {
        $query ="SELECT * FROM cat";
        $result = $this->connect()->query($query);
        if($result->rowCount() > 0)
        {
            while ($row = $result->fetch()) {
                $c = new Category;
                $c->id = $row['id'];
                $c->name = $row['name'];
                $c->att = $row['att'];
                $c->unit = $row['unit'];
                $data[] = $c;
            }
            return $data;
        }
    }
    public function FillSelect()
    {
        $sql = "SELECT id,name FROM cat";
        $res = $this->connect()->query($sql);
        if($res->rowCount() > 0)
        {
            while ($row = $res->fetch()) {
                $id = $row['id'];
                $name = $row['name'];
                echo "<option value='$id'>$name</option>";
            }
        } 
    }
}
?>