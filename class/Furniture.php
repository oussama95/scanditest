<?php
require_once("Product.php");
class Furniture extends Product
{
    private $HWL;

    public function __construct($SKU, $name, $price, $catId, $HWL)
    {
        $this->name = $name;
        $this->SKU = $SKU;
        $this->price = $price;
        $this->catId = $catId;
        $this->HWL = $HWL;
    }
    
    public function GetHWL()
    {
        return $this->HWL;
    }
    public function Save() //Saving the data od the abject in the item table
	{
		try {
			if ($this->checkSKU() == true) {
				$sql = "INSERT INTO item (SKU, name, price, properties, catId) 
						VALUES ('$this->SKU', '$this->name', $this->price, '$this->HWL', $this->catId)";
				$res = $this->connect()->prepare($sql);
				$res->execute();
				header("Location(index.php)"); //if done go to index
			} else //if we have dublicate SKU in the database so go back to enter new item
			{
				echo "<script>
							alert('SKU is already found in the database');
							window.location.href='add.php';
							</script>";
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function checkSku() //To Check if the sku already exicts in the database
	{
		try {
			$sku = $this->SKU;
			$sql = "SELECT SKU FROM item WHERE SKU = :id";
			$res = $this->connect()->prepare($sql);
			$res->bindParam(':id', $sku);
			$res->execute();
			if ($res->rowCount() > 0) {
				return false;
			}
			return true;
		} catch (Exception $e) {
			echo "EXEPTION IN CHECK          " . $e->getMessage();
			return false;
		}
	}
}
?>