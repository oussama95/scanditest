<?php
require_once('Database.php');
require_once('Cat.php');
class Product extends Database
{
	protected $SKU;
	protected $name;
	protected $price;
	protected $catId;
	protected $unit;
	protected $caption;

	public function GetSKU()
    {
        return $this->SKU;
    }
    
    public function Getname()
    {
        return $this->name;
    }
    
    public function GetPrice()
    {
        return $this->price;
    }
    
    public function GetCatId()
    {
        return $this->catId;   
    }
	public function SetUnit($unit)
	{
		$this->unit = $unit;
	}
	public function SetCaption($cap)
	{
		$this->caption = $cap;
	}
	public function GetUnit()
	{
		return $this->unit;
	}
	public function GetCaption()
	{
		return $this->caption;
	}
}
