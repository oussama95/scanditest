<?php 
require_once("Database.php");
require_once("DVD.php");
require_once("Book.php");
require_once("Furniture.php");
class Action extends Database{

    public function getProducts()
	{
		$sql = "Select * From Item";
		$result = $this->connect()->query($sql);
		$cat = new Category;
		$catList = $cat->GetCat();
		if ($result->rowCount() > 0) {
			while ($row = $result->fetch()) {
                $cat = $row['catId'];
                
                if($cat == 1)
                {
                    $dvd = new DVD($row['SKU'],$row['name'],$row['price'],$row['catId'],$row['properties']);
                    $dvd->SetUnit($catList[0]->unit);
                    $dvd->SetCaption($catList[0]->att);
                    $data[] = $dvd;
                }
                else if ($cat == 2)
                {
                    $book = new Book($row['SKU'],$row['name'],$row['price'],$row['catId'],$row['properties']);
                    $book->SetUnit($catList[1]->unit);
                    $book->SetCaption($catList[1]->att);
                    $data[] = $book;
                }
                else if ($cat == 3)
                {
                    $fur = new Furniture($row['SKU'],$row['name'],$row['price'],$row['catId'],$row['properties']);
                    $fur->SetUnit($catList[2]->unit);
                    $fur->SetCaption($catList[2]->att);
                    $data[] = $fur;
                }
			}

			return $data;
		}
    }
    public function DeleteProducts(array $item)
	{
		$str = "";
		foreach ($item as $row) {
			$str .= "'" . $row . "'";
			if (next($item) == true) {
				$str .= ",";
			}
		}
		$sql = "DELETE FROM item WHERE SKU IN ($str)";
		$res = $this->connect()->prepare($sql);
		$res->execute();
    }
}
?>