<?php
require_once("class/Cat.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
                           integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="my_style.css">

    <title>Product Add</title>
</head>

<body>
<div class= "container">    
    <nav class="navbar navbar-expand-lg topbar">
        <br>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <h1>
                Product List
            </h1>
        </div>
        <form method="post" action="action.php">
        <div class="row">
            <div class="form-group">
                <input type="submit" class="" name = "add-submit" value = "Apply">
            </div>
        </div>
        </div>
    </nav>
        <div class="col add-form">
            <div class = "row ">
                <label> SKU </label>
                <input type = "text" name = "sku" required>
            </div>
            <div class = "row">
                <label> Name </label>
                <input type = "text" name = "name" required>
            </div>
            <div class = "row">
                <label> Price </label>
                <input type = "text" pattern="\d*" name = "price" required>
            </div>
            <div class = "row">
            <label>Type</label>
            <select name = "type" id = "type">
                <?php 
                $cat = new Category;
                $cat->FillSelect();
                ?>
            </select>
            </div>
            <div class="" id = "select-dvd">
                <div class = "row">
                    <label> Size </label>
                    <input type = "text" pattern="\d*" name = "size" id = "size" >
                </div>
                <div class="info">
                    <p>Please fill the size of the DVD in MB</p>
                </div>
            </div>

            <div class="" id = "select-book">
                <div class = "row">
                    <label> Weight </label>
                    <input type = "text" pattern="\d*" name = "weight" id = "weight" >
                </div>
                <div class="info">
                    <p>Please fill the weight of the book In KG</p>
                </div>
            </div>

            <div class="" id = "select-fur">
                <div class = "row">
                    <label> Height </label>
                    <input type = "text" pattern="\d*" name = "height" id = "height">
                </div>
                <div class = "row">
                    <label> Width </label>
                    <input type = "text" pattern="\d*" name = "width" id = "width" >
                </div>
                <div class = "row">
                    <label> Length </label>
                    <input type = "text" pattern="\d*" name = "length" id = "length">
                </div>
                <div class="info">
                    <p>Please fill the dimentions in HxWxL format of the Furniture</p>
                </div>
            </div>

   
        </div>  
    
</div>  </form>
<script type="text/javascript" src="includes/script.js"></script>
    
</body>

</html> 
  