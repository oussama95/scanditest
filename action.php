<?php
require_once("class/action.php");

    if(isset($_POST['select'])){ // When coming from index
        $select = $_POST['select']; //selected option

        if($select == 1)
        {   
            if(isset($_POST['sku']))
            {
                    $checked = $_POST['sku']; //array of selected items
                    $action = new Action;
                    $action->DeleteProducts($checked);
                    echo "<script>
                            alert('Done!');
                            window.location.href='index.php';
                            </script>";
            }else
            {
                echo "<script>
                alert('No selected Items to delete');
                window.location.href='index.php';
                </script>";
            }
        }else if($select == 2)
        {
            header("Location:add.php");
        }
    }


    if(isset($_POST['add-submit']))// if coming from add.php
    {
            $name = $_POST['name'];
            $sku = $_POST['sku'];
            $price = $_POST['price'];
            $catId = $_POST['type'];
            $prop = "";
            if( $catId == 1)
            {   
                $prop = $_POST['size'];
                $dvd = new DVD($sku, $name, $price,$catId,$prop);
                $dvd->Save();
            }
            elseif ($catId == 2) 
            {
                $prop = $_POST['weight'];
                $book = new Book($sku, $name, $price,$catId,$prop);
                $book->Save();
            }
            elseif ($catId == 3)
            {   
                $prop = $_POST['height'] . "x" . $_POST['width'] . "x" . $_POST['length'];
                $fur = new Furniture($sku, $name, $price,$catId,$prop);
                $fur->Save();
            }
            echo "<script>
                alert('Done');
                window.location.href='index.php';
                </script>";
    }

?>