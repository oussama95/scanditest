$(function()
{
        showdivs();
    $('#type').on('change',function()
    {   
        showdivs();
    });
});

function showdivs()
{
    if($('#type').val() == '1')
    {   
        
        $('#select-dvd').show();
        $('#select-book').hide();
        $('#select-fur').hide();
        $("#size").prop('required',true);
        $("#weight").prop('required',false);
        $("#height").prop('required',false);
        $("#length").prop('required',false);
        $("#width").prop('required',false);
    }
    else if($('#type').val() == '2')
    {
        $('#select-dvd').hide();
        $('#select-book').show();
        $('#select-fur').hide();
        $("#size").prop('required',false);
        $("#weight").prop('required',true);
        $("#height").prop('required',false);
        $("#length").prop('required',false);
        $("#width").prop('required',false);

    }
    else if($('#type').val() == '3')
    {   
        $('#select-dvd').hide();
        $('#select-book').hide();
        $('#select-fur').show();
        $("#size").prop('required',false);
        $("#weight").prop('required',false);
        $("#height").prop('required',true);
        $("#length").prop('required',true);
        $("#width").prop('required',true);

    }
}