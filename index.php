<?php
require_once("class/action.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="my_style.css">

    <title>Product List</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg topbar">
        <br>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <h1>
                Product List
            </h1>
        </div>
        <form method="post" action="action.php">
            <div class="row">
                <div class="form-group">
                    <select class="" name="select">
                        <option value="1">Mass delete action </option>
                        <option value="2">Add Action</option>
                        <option value="3">Edit Action </option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="" name="submit" value="Apply">
                </div>
            </div>
            </div>
    </nav>
    <div class="container">
        <?php
        $action = new Action;
        $Products = $action->getProducts();
        $count = 0;
        foreach ($Products as $obj) {
            if (get_class($obj) == 'DVD') 
            {
                if ($count == 0) 
                {
                    echo "<div class='row'>";
                }
                ?>
                <div class='col-3 '>
                    <div class='card'>
                        <div class='card-body'>
                        	<input type='checkbox' name = 'sku[]' value = '<?php echo $obj->GetSKU(); ?>' >
                        		<div class='card-detail'>
                        			<h5 class='card-title'><?php echo $obj->GetSKU(); ?></h5>
                        			<h6 class='card-subtitle mb-2 '><?php echo $obj->GetName(); ?></h6>
                        			<h6 class='card-subtitle mb-2 '><?php echo $obj->GetPrice(); ?>$</h6>
                        			<p class='card-text'><?php echo $obj->GetCaption() ." : ". $obj->GetSize() ." ". $obj->GetUnit();?></p>
                        		</div>
                        	</div>
                        </div>
                    </div>
                    <?php
                $count++;
            }else if (get_class($obj) == 'Book') 
            {
                if ($count == 0) 
                {
                    echo "<div class='row'>";
                }
                ?>
                <div class='col-3 '>
                    <div class='card'>
                        <div class='card-body'>
                        	<input type='checkbox' name = 'sku[]' value = '<?php echo $obj->GetSKU(); ?>' >
                        		<div class='card-detail'>
                        			<h5 class='card-title'><?php echo $obj->GetSKU(); ?></h5>
                        			<h6 class='card-subtitle mb-2 '><?php echo $obj->GetName(); ?></h6>
                        			<h6 class='card-subtitle mb-2 '><?php echo $obj->GetPrice(); ?>$</h6>
                        			<p class='card-text'><?php echo $obj->GetCaption() ." : ". $obj->GetWeight() ." ". $obj->GetUnit();?></p>
                        		</div>
                        	</div>
                        </div>
                    </div>
                    <?php
                $count++;
            }
            else if (get_class($obj) == 'Furniture') 
            {
                if ($count == 0) 
                {
                    echo "<div class='row'>";
                }
                ?>
                <div class='col-3 '>
                    <div class='card'>
                        <div class='card-body'>
                        	<input type='checkbox' name = 'sku[]' value = '<?php echo $obj->GetSKU(); ?>' >
                        		<div class='card-detail'>
                        			<h5 class='card-title'><?php echo $obj->GetSKU(); ?></h5>
                        			<h6 class='card-subtitle mb-2 '><?php echo $obj->GetName(); ?></h6>
                        			<h6 class='card-subtitle mb-2 '><?php echo $obj->GetPrice(); ?>$</h6>
                        			<p class='card-text'><?php echo $obj->GetCaption() ." : ". $obj->GetHWL() ." ". $obj->GetUnit();?></p>
                        		</div>
                        	</div>
                        </div>
                    </div>
                    <?php
                $count++;
            }
            if ($count == 4) {
                $count = 0;
                echo "</div>";
            }
        }
        if ($count > 0) {
            echo "</div>"; //to close the opened row 
        }
        ?>
    </div>
    </form>
</body>

</html>